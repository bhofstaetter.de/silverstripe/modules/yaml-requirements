<?php

namespace bhofstaetter\YamlRequirements;

use Psr\SimpleCache\CacheInterface;
use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\Control\Director;
use SilverStripe\Core\ClassInfo;
use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Core\Flushable;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Core\Manifest\ModuleResourceLoader;
use SilverStripe\Core\Path;
use SilverStripe\View\Parsers\URLSegmentFilter;
use SilverStripe\View\ThemeResourceLoader;

class Collector implements Flushable {
    use Configurable;

    public static function inst(): Collector {
        return new Collector();
    }

    public function collectRequirements(string $type, string $className): array {
        $cache = Injector::inst()->get(CacheInterface::class . '.YamReqCache');

        $cacheKey = implode('__', [
            strtolower($this->sanitiseClassName($className)),
            $type,
            Director::host(),
        ]);

        if ($cache->has($cacheKey) && !Director::isDev()) {
            return $cache->get($cacheKey);
        } else {
            $config = Config::inst();

            if ($config->get($className, 'yamReq_block_' . $type)) {
                $requirements = [
                    'single' => [],
                    'combined' => [],
                    'preConnect' => [],
                ];

                $cache->set($cacheKey, $requirements);

                return $requirements;
            }

            $requirements = $this->getRequirementsForController($className, $type);
            $requirements = $this->processRequirements($requirements);
            $cache->set($cacheKey, $requirements);

            return $requirements;
        }
    }

    private function getRequirementsForController(string $className, string $type): array {
        $requirements = [];
        $config = Config::inst();

        foreach (array_reverse(ClassInfo::ancestry($className)) as $className) {
            $unInherited = $config->get($className, 'yamReq_block_inherited_' . $type);

            $requirements = array_merge(
                (array) $config->get($className, 'yamReq_' . $type, $unInherited ? Config::UNINHERITED : 0),
                $requirements
            );

            if ($unInherited || $className === ContentController::class) {
                break;
            }
        }

        return $requirements;
    }

    private function processRequirements(array $requirements): array {
        $requirements = $this->ensureDataIntegrity($requirements);
        $requirements = $this->removeBlockedRequirements($requirements);
        $requirements = $this->mergeRequirementsWithSameIdentifier($requirements);

        return $this->resolveGroupAndSortRequirements($requirements);
    }

    private function ensureDataIntegrity(array $requirements): array {
        $config = self::config();

        foreach ($requirements as $key => $options) {
            if (!isset($options['identifier'])) {
                $requirements[$key]['identifier'] = $key;
            }

            if (!isset($options['version'])) {
                $requirements[$key]['version'] = '0';
            }

            if (!isset($options['include'])) {
                $requirements[$key]['include'] = 'single';
            }

            if (!isset($options['order'])) {
                $requirements[$key]['order'] = $config->get('default_order');
            }

            if (!isset($options['resolve'])) {
                $requirements[$key]['resolve'] = $config->get('default_resolve');
            }

            if (!isset($options['preload'])) {
                $requirements[$key]['preload'] = $config->get('default_preload');
            }

            if (!isset($options['download'])) {
                $requirements[$key]['download'] = $config->get('default_download');
            }

            if (!isset($options['preConnect'])) {
                $requirements[$key]['preConnect'] = $config->get('default_preConnect');
            }
        }

        return $requirements;
    }

    private function removeBlockedRequirements(array $requirements): array {
        foreach ($requirements as $key => $options) {
            if ($options['include'] === 'block') {
                unset($requirements[$key]);
            }
        }

        return $requirements;
    }

    private function mergeRequirementsWithSameIdentifier(array $requirements): array {
        $requirementsByIdentifier = [];

        foreach ($requirements as $key => $options) {
            $identifier = $options['identifier'];

            // first with this identifier
            if (!isset($requirementsByIdentifier[$identifier])) {
                $options['src'] = $key;
                $requirementsByIdentifier[$identifier] = $options;
                continue;
            }

            $requirementWithSameIdentifier = $requirementsByIdentifier[$identifier];

            // highest order wins
            if ($options['order'] > $requirementWithSameIdentifier['order']) {
                $requirementsByIdentifier[$identifier]['order'] = $options['order'];
            } else {
                $options['order'] = $requirementWithSameIdentifier['order'];
            }

            // highest version wins
            if (version_compare($options['version'], $requirementWithSameIdentifier['version'], '>')) {
                $requirementsByIdentifier[$identifier] = $options;
                $requirementsByIdentifier[$identifier]['src'] = $key;
            }

            unset($requirements[$key]);
        }

        foreach ($requirementsByIdentifier as $requirement) {
            $src = $requirement['src'];
            unset($requirement['src']);

            $requirements[$src] = $requirement;
        }

        return $requirements;
    }

    private function resolveGroupAndSortRequirements(array $requirements): array {
        $singleRequirements = [];
        $combinedRequirements = [];
        $preConnectTargets = [];

        $themeLoader = ThemeResourceLoader::inst();
        $moduleLoader = ModuleResourceLoader::singleton();

        $downloadPath = PUBLIC_PATH . '/' . RESOURCES_DIR . '/yamReqsCache';

        if (!file_exists($downloadPath)) {
            mkdir($downloadPath, 0755, true);
        }

        foreach ($requirements as $src => $options) {
            if ($options['resolve'] === 'theme') {
                $filePath = Path::join(RESOURCES_DIR, $themeLoader->findThemedResource($src));
            } else if ($options['resolve'] === 'module') {
                $filePath = Path::join(RESOURCES_DIR, $moduleLoader->resolvePath($src));
            } else {
                $filePath = $src;

                if ($this->isExternalUrl($filePath)) {
                    $urlParts = parse_url($filePath);
                    $target = $urlParts['scheme'] . '://' . $urlParts['host'];

                    if ($options['download']) {
                        if ($downloadedFilePath = $this->downloadExternalFile($filePath, $downloadPath)) {
                            $filePath = $downloadedFilePath;
                        } else {
                            $options['include'] = 'single';

                            if ($options['preConnect']) {
                                $preConnectTargets[$target] = $target;
                            }
                        }
                    } else {
                        $options['include'] = 'single';

                        if ($options['preConnect']) {
                            $preConnectTargets[$target] = $target;
                        }
                    }
                } else {
                    $filePath = '/' . $filePath;
                }
            }

            if (
                !$filePath
                || (!$this->isExternalUrl($filePath) && !$this->fileExistsAtPath($filePath))
            ) {
                continue;
            }

            $options['include'] === 'single'
                ? $singleRequirements[] = ['filePath' => $filePath, 'options' => $options]
                : $combinedRequirements[$filePath] = $options['order'];
        }

        if (Director::isDev()) {
            foreach ($singleRequirements as &$singleRequirement) {
                $filePath = $singleRequirement['filePath'];

                if (!$this->isExternalUrl($filePath)) {
                    $singleRequirement['filePath'] = $filePath . '?m=' . sha1_file($filePath);
                }
            }
        }

        usort($singleRequirements, function ($item1, $item2) {
            return $item1['options']['order'] <=> $item2['options']['order'];
        });

        asort($combinedRequirements, SORT_NUMERIC);

        return [
            'single' => $singleRequirements,
            'combined' => array_keys($combinedRequirements),
            'preConnect' => $preConnectTargets,
        ];
    }

    private function fileExistsAtPath(string $filePath) {
        $parts = [
            BASE_PATH,
            PUBLIC_DIR,
            $filePath,
        ];

        return file_exists(Path::join($parts));
    }

    private function isExternalUrl(string $filePath): bool {
        return str_starts_with($filePath, '//') || str_starts_with($filePath, 'http');
    }

    private function downloadExternalFile(string $url, string $targetPath): string|bool {
        $ctx = stream_context_create(['http' => ['timeout' => 1]]);
        $filter = URLSegmentFilter::create();

        $fileName = basename($url);
        $fileName = $targetPath . '/' . $filter->filter(str_replace($fileName, '', $url)) . '_' . $fileName;

        $contents = @file_get_contents($url, false, $ctx);

        if ($contents !== false) {
            $written = @file_put_contents($fileName, $contents);

            if ($written !== false) {
                return str_replace(BASE_PATH . '/' . PUBLIC_DIR . '/', '', $fileName);
            }
        }

        return false;
    }

    private function sanitiseClassName($className): string {
        return str_replace('\\', '-', $className);
    }

    public static function flush(): void {
        Injector::inst()->get(CacheInterface::class . '.YamReqCache')->clear();
    }
}
