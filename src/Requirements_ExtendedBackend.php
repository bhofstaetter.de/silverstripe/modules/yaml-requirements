<?php

namespace bhofstaetter\YamlRequirements;

use SilverStripe\View\HTML;
use SilverStripe\View\Requirements_Backend;

class Requirements_ExtendedBackend extends Requirements_Backend {

    public function css($file, $media = null, $options = []) {
        parent::css($file, $media, $options);

        if (isset($options['preload']) && $options['preload']) {
            $this->unsetCSS($file);
            self::insertHeadTags(self::css_preload_tag($file, $options));
        }
    }

    public function javascript($file, $options = []) {
        parent::javascript($file, $options);

        if (isset($options['preload']) && $options['preload']) {
            $type = null;

            if (isset($this->javascript[$file]['type'])) {
                $type = $this->javascript[$file]['type'];
            }

            if (isset($options['type'])) {
                $type = $options['type'];
            }

            $this->unsetJavascript($file);
            self::insertHeadTags(self::javascript_preload_tag($file, $options, $type));
        }
    }

    public function processCombinedFiles() {
        parent::processCombinedFiles();

        foreach ($this->css as $file => $options) {
            if (str_contains($file, 'yamreq-bundled')) {
                $this->unsetCSS($file);
                self::insertHeadTags(self::css_preload_tag($file, $options));
                break;
            }
        }

        foreach ($this->javascript as $file => $options) {
            if (str_contains($file, 'yamreq-bundled')) {
                $this->unsetJavascript($file);
                self::insertHeadTags(self::javascript_preload_tag($file, $options, 'javascript'));
                break;
            }
        }
    }

    private static function css_preload_tag(string $file, array $options = []): string {
        return HTML::createTag('link', [
            'rel' => 'preload',
            'as' => 'style',
            'type' => 'text/css',
            'onload' => "this.rel='stylesheet'",
            'href' => $file,
            'media' => $options['media'] ?? 'screen',
            'integrity' => $options['integrity'] ?? null,
            'crossorigin' => $options['crossorigin'] ?? null,
        ]);
    }

    private static function javascript_preload_tag(string $file, array $options = [], string $type = null): string {
        return HTML::createTag('link', [
            'rel' => 'preload',
            'as' => 'script',
            'href' => $file,
            'type' => $type,
            'integrity' => $options['integrity'] ?? null,
            'crossorigin' => $options['crossorigin'] ?? null,
        ]);
    }
}
