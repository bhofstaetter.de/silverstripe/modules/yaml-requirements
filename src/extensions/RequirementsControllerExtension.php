<?php

namespace bhofstaetter\YamlRequirements;

use SilverStripe\Core\Extension;
use SilverStripe\View\HTML;
use SilverStripe\View\Requirements;
use SilverStripe\View\Requirements_Backend;

class RequirementsControllerExtension extends Extension {

    protected Collector $collector;
    protected Requirements_Backend $backend;

    public function onBeforeInit(): void {
        if ($this->getOwner()->config()->get('yamReq_clear_all_regular')) {
            Requirements::clear();
        }

        $this->setCollector();
        $this->startInclusion();
    }

    private function setCollector() {
        $this->backend = Requirements::backend()
            ->setWriteHeaderComment(false)
            ->setWriteJavascriptToBody(true)
            ->setForceJSToBottom(true);

        $this->collector = Collector::inst();
    }

    private function startInclusion(): void {
        $className = get_class($this->owner);

        $this->includeRequirements(
            $this->collector->collectRequirements('css', $className),
            'css',
        );

        $this->includeRequirements(
            $this->collector->collectRequirements('javascript', $className),
            'javascript',
        );
    }

    private function includeRequirements(array $requirements, string $type) {
        foreach ($requirements['preConnect'] as $preConnectTarget) {
            $this->backend->insertHeadTags(HTML::createTag('link', [
                'rel' => 'preconnect',
                'href' => $preConnectTarget,
            ]));
        }

        foreach ($requirements['single'] as $requirement) {
            $type === 'css'
                ? Requirements::css($requirement['filePath'], null, $requirement['options'])
                : Requirements::javascript($requirement['filePath'], $requirement['options']);
        }

        if (count($requirements['combined'])) {
            Requirements::combine_files(
                'yamreq-bundled.' . ($type === 'css' ? 'css' : 'js'),
                $requirements['combined']
            );
        }
    }
}
