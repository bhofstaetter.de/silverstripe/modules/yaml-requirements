# Silverstripe Yaml Requirements

include and replace requirements through yaml configuration and a few more opinionated improvements on requirements

## Installation

```
composer require bhofstaetter/yaml-requirements
```

## Why?

* To have an easy way to overwrite and edit requirements no matter where they come from. Globally and on per page basis.
* To prevent multiple versions of the same library (e.g. jQuery) being included.
* To adjust the order of requirements no matter where they come from depending on the needs of the project or the current page.
* To be able to deliver external requirements locally.
* To have some SEO improvements

## Usage

This module works similarly to the inclusion of requirements through a controller via Requirements::js/javascript(...).
Requirements must be stored and configured in the YAML configuration for the desired page controller.

* css: ``My\Cool\PageController.yamReq_css``
* javascript: ``My\Cool\PageController.yamReq_javascript``

a simple but *not recommendable* example, that works because of preset defaults for each requirement

```yaml
My\Cool\PageController:
    yamReq_css:
        https://external-domain.tld/some-styling.css:
        file-in-public-dir.css:
```

All requirements should have at least one ``identifier`` and probably one ``version``.
This way the requirement can be easily exchanged and modified. Additionally, this prevents multiple versions of a resource
being included (jquery 1.7, jquery 1.8, ...). Only the latest version for the given ident will be used.

```yaml
My\Cool\PageController:
    yamReq_css:
        https://external-domain.tld/some-styling.css:
            identifier: 'some-styling'
            version: '1.0.0'
        file-in-public-dir.css:
            identifier: 'shouldnt-be-here'
```

### Requirement from theme

```yaml
My\Cool\PageController:
    yamReq_css:
        client/dist/css/styles.css:
            identifier: 'theme-styles'
            resolve: 'theme'
```

### Requirement from module

```yaml
My\Cool\PageController:
    yamReq_css:
        vendor/module-name:client/dist/css/warnings.css:
            identifier: 'module-name-warnings'
            resolve: 'module'
```

### Available requirement configuration options

* ``identifier``: can be used to overwrite requirements. Multiple requirements with the same identifier will be merged together. Highest version wins. Highest order wins.
* ``version``: version of this file, following Semver. You could you '1.999.999' or similar to say this is probably the highest version of this requirement and overwrite all other
* ``include``: single | combined | block
* ``order``: a number to define the inclusion order of a requirement
* ``resolve``: none | module | theme (none is used for files directly inside the public folder and external resources)
* ``preload``: true | false
* ``download``: true | false (try to download external resources and server them locally, even combined)
* ``preConnect``: true | false

## Configuration

### Remove all other requirements

Removes all requirements that were not included via YamlRequirements module

```yaml
bhofstaetter\YamlRequirements\RequirementsControllerExtension:
    yamReq_clear_all_regular: true # default is false
```

### Set some defaults for a single requirement

```yaml
bhofstaetter\YamlRequirements\Collector:
    default_download: true # try to download an external file and serve it locally
    default_preload: false # insert preload tag for the requirement instead of a regular link
    default_preConnect: true # add a preconnect link for an external resource
    default_resolve: 'none'
    default_order: 500

```

### Block requirements on page level

```yaml
My\Cool\PageController:
    yamReq_block_inherited_css: true # prevents the inclusion from all parent controllers css requirements
    yamReq_block_inherited_javascript: true  # prevents the inclusion from all parents controllers js requirements
    yamReq_block_css: true # prevents the inclusion from any css
    yamReq_block_javascript: true # prevents the inclusion from any js
```

## Todo

- Error Logging to Logfile
- Order "Shortcuts" like DefaultModuleOrder (500) or DefaultThemeOrder(300) or DefaultCustomOrder(100)
- transform regular requirements to yaml requirements (task to list them?)
- task to download external requirements?
- check when a downloaded file will be refreshed
